<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */



    public function up()
    {

        Schema::create('products', function (Blueprint $table) {



            $table->id('tableID')->autoIncrement();
            $table->string('id');
            $table->string('organizationId');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('accountId');
            $table->string('productNo');
            $table->string('suppliersProductNo');
            $table->string('salesTaxRulesetId');
            $table->string('isArchived');
            $table->string('prices')->default(0);
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
