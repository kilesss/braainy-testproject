<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Contacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id('tableID')->autoIncrement();
            $table->string('id');
            $table->string('type');
            $table->string('organizationId');
            $table->timestamp('createdTime')->useCurrent();
            $table->string('name');
            $table->string('countryId');
            $table->string('street')->nullable();
            $table->integer('cityId')->nullable();
            $table->string('cityText')->nullable();
            $table->integer('stateId')->nullable();
            $table->string('stateText')->nullable();
            $table->string('zipcodeText')->nullable();
            $table->string('contactNo')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('currencyId')->nullable();
            $table->string('registrationNo')->nullable();
            $table->string('ean')->nullable();
            $table->integer('localeId')->nullable();
            $table->boolean('isCustomer');
            $table->boolean('isSupplier');
            $table->string('paymentTermsMode')->nullable();
            $table->string('paymentTermsDays')->nullable();
            $table->string('accessCode');
            $table->string('emailAttachmentDeliveryMode')->nullable();
            $table->boolean('isArchived');
            $table->boolean('isSalesTaxExempt');
            $table->string('defaultExpenseProductDescription')->nullable();
            $table->string('defaultExpenseAccountId')->nullable();
            $table->string('defaultTaxRateId')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
