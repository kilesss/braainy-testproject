@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}

                        <a class="btn btn-info btn-sm my-0" style="float: right"
                           href="{{ route('contacts/sync') }}">{{ __('Sync contacts') }}</a>

                        <a type="button" style="float: right" class="btn btn-info btn-sm my-0"
                                data-toggle="modal" data-target="#createContacts">
                            Create contacts
                        </a>
                    </div>
                    <!-- Button trigger modal -->


                    <!-- Modal -->

                    <div class="card-body">
                        <table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('name')}}</th>
                                <th>{{__('actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(isset($contacts))
                                @foreach($contacts  as $k =>  $cont)
                                    <tr>
                                        <td>{{$k}}</td>
                                        <td>{{$cont->name}}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-id="{{$cont->tableID}}"
                                                    data-toggle="modal" id="editContactBTN">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                        <div class="modal fade" id="editContact" tabindex="-1" aria-labelledby="editContact"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit contact</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form class="form-vertical" action="{{ route('contacts/edit') }}" method="post">
                                        @csrf
                                    <div class="modal-body">
                                        <div class="modal-body" id="editContactForm">
                                                                                 </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="createContacts" tabindex="-1" aria-labelledby="createContacts"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit contact</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form class="form-vertical" action="{{ route('contacts/create') }}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                                <div class="form-group">
                                                    <label>type</label>
                                                    <input type="text" name="type" id="type" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>organizationId</label>
                                                    <input type="text" name="organizationId" id="organizationId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>name</label>
                                                    <input type="text" name="name" id="name" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>countryId</label>
                                                    <input type="text" name="countryId" id="countryId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>street</label>
                                                    <input type="text" name="street" id="street" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>cityId</label>
                                                    <input type="text" name="cityId" id="cityId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>cityText</label>
                                                    <input type="text" name="cityText" id="cityText" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>stateId</label>
                                                    <input type="text" name="stateId" id="stateId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>stateText</label>
                                                    <input type="text" name="stateText" id="stateText" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>zipcodeText</label>
                                                    <input type="text" name="zipcodeText" id="zipcodeText" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>contactNo</label>
                                                    <input type="text" name="contactNo" id="contactNo" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>phone</label>
                                                    <input type="text" name="phone" id="phone" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>fax</label>
                                                    <input type="text" name="fax" id="fax" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>currencyId</label>
                                                    <input type="text" name="currencyId" id="currencyId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>registrationNo</label>
                                                    <input type="text" name="registrationNo" id="registrationNo" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>ean</label>
                                                    <input type="text" name="ean" id="ean" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>localeId</label>
                                                    <input type="text" name="localeId" id="localeId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>isCustomer</label>
                                                    <input type="text" name="isCustomer" id="isCustomer" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>isSupplier</label>
                                                    <input type="text" name="isSupplier" id="isSupplier" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>paymentTermsMode</label>
                                                    <input type="text" name="paymentTermsMode" id="paymentTermsMode" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>paymentTermsDays</label>
                                                    <input type="text" name="paymentTermsDays" id="paymentTermsDays" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>accessCode</label>
                                                    <input type="text" name="accessCode" id="accessCode" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>emailAttachmentDeliveryMode</label>
                                                    <input type="text" name="emailAttachmentDeliveryMode" id="emailAttachmentDeliveryMode" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>isArchived</label>
                                                    <input type="text" name="isArchived" id="isArchived" value="0">
                                                </div>

                                                <div class="form-group">
                                                    <label>isSalesTaxExempt</label>
                                                    <input type="text" name="isSalesTaxExempt" id="isSalesTaxExempt" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>defaultExpenseProductDescription</label>
                                                    <input type="text" name="defaultExpenseProductDescription" id="defaultExpenseProductDescription" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>defaultExpenseAccountId</label>
                                                    <input type="text" name="defaultExpenseAccountId" id="defaultExpenseAccountId" value="">
                                                </div>

                                                <div class="form-group">
                                                    <label>defaultTaxRateId</label>
                                                    <input type="text" name="defaultTaxRateId" id="defaultTaxRateId" value="">
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                                </button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>


            $(document).on('click', "#editContactBTN", function () {
                var id = $(this).data("id");

                $.ajax({
                    type: 'GET',
                    url: '{!! url('/contacts/getContacts') !!}',
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        var htmlBody = '<input type="hidden" name="tableID" value="'+id+'">'

                        $.each(data, function( index, value ) {
                            if(value == null){
                                value = '';
                            }
                            htmlBody +='  <div class="form-group">'+
                                    '<label>'+index+'</label>'+
                                    '<input type="text" name="'+index+'" id="'+index+'" value="'+value+'">'+
                                    '</div> ';
                        });
                        $("#editContactForm").html(htmlBody);
                        $("#editContact").modal('show')
                    }
                });
            });
        </script>
@endsection
