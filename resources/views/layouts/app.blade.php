<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Braainy') }}</title>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <nav class="navbar fixed-top navbar-expand-md navbar-light white double-nav scrolling-navbar">

            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i><span class="sr-only" aria-hidden="true">Toggle side navigation</span></a>
            </div>
            <!-- Navbar links-->
            <div class="mr-auto pl-2">
    <span class="d-none d-md-inline-block">


        <!--jQuery-->
        <!--DC content-->
        <div id="dpl-gtm-jq">
            <span id="gtmDC-navbar-unlogged" style="min-height:1px" data-gtm-vis-polling-id-2340190_1459="421"></span>

            <a class="btn btn-info btn-sm my-0" href="{{ route('products') }}">{{ __('Products') }}</a>
            <a class="btn btn-info btn-sm my-0" href="{{ route('contacts') }}">{{ __('Contacts') }}</a>

        </div>
        <!--/DC content-->


    </span>
            </div>

            <!--Navigation icons-->
  <span id="dpl-navbar-right-buttons">


<!--Navigation icons-->
<ul class="nav navbar-nav nav-flex-icons ml-auto">


    <!-- Authentication Links -->
    @guest
    <li class="nav-item">
        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
    </li>
    @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
    @endif
    @else

        <li>
            <a class="dropdown-item logoutBtn" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
            </form>
        </li>
        @endif
</ul>
</span>
        </nav>
        @if($errors->any())
            @foreach($errors->all() as $err)
                <div class="alert alert-error">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong> </strong> {{$err}}
                </div>

            @endforeach
        @endif
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
