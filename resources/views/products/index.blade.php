@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}

                        <a class="btn btn-info btn-sm my-0" style="float: right"
                           href="{{ route('products/sync') }}">{{ __('Sync products') }}</a>

                        <a type="button" style="float: right" class="btn btn-info btn-sm my-0"
                           data-toggle="modal" data-target="#createProducts">
                            Create products
                        </a>
                    </div>
                    <!-- Button trigger modal -->


                    <!-- Modal -->

                    <div class="card-body">
                        <table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{__('name')}}</th>
                                <th>{{__('actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(isset($products))
                                @foreach($products  as $k =>  $cont)
                                    <tr>
                                        <td>{{$k}}</td>
                                        <td>{{$cont->name}}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-id="{{$cont->tableID}}"
                                                    data-toggle="modal" id="editProductBTN">
                                                Edit
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>

                        </table>
                        <div class="modal fade" id="editProduct" tabindex="-1" aria-labelledby="editProduct"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form class="form-vertical" action="{{ route('products/edit') }}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="modal-body" id="editProductForm">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                                </button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="createProducts" tabindex="-1" aria-labelledby="createProducts"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Product</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form class="form-vertical" action="{{ route('products/create') }}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>organizationId</label>
                                                <input type="text" name="organizationId" id="organizationId" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>name</label>
                                                <input type="text" name="name" id="name" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>description</label>
                                                <input type="text" name="description" id="description" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>accountId</label>
                                                <input type="text" name="account" id="account" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>productNo</label>
                                                <input type="text" name="productNo" id="productNo" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>suppliersProductNo</label>
                                                <input type="text" name="suppliersProductNo" id="suppliersProductNo" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>salesTaxRulesetId</label>
                                                <input type="text" name="salesTaxRulesetId" id="salesTaxRulesetId" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>isArchived</label>
                                                <input type="text" name="isArchived" id="isArchived" value="">
                                            </div>

                                            <div class="form-group">
                                                <label>prices</label>
                                                <input type="text" name="prices" id="prices" value="">
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>


        $(document).on('click', "#editProductBTN", function () {
            var id = $(this).data("id");

            $.ajax({
                type: 'GET',
                url: '{!! url('/products/getProducts') !!}',
                data: {
                    id: id,
                },
                success: function (data) {
                    var htmlBody = '<input type="hidden" name="tableID" value="'+id+'">'

                    $.each(data, function( index, value ) {
                        if(value == null){
                            value = '';
                        }
                        htmlBody +='  <div class="form-group">'+
                                '<label>'+index+'</label>'+
                                '<input type="text" name="'+index+'" id="'+index+'" value="'+value+'">'+
                                '</div> ';
                    });
                    $("#editProductForm").html(htmlBody);
                    $("#editProduct").modal('show')
                }
            });
        });
    </script>
@endsection
