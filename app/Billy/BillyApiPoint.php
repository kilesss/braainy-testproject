<?php
/**
 * Created by PhpStorm.
 * User: kilesss
 * Date: 10/10/2020
 * Time: 4:25 PM
 */


namespace App\Billy;
ini_set('memory_limit', '-1');

use Illuminate\Support\Facades\Config;
use TheSeer\Tokenizer\Exception;


/**
 * Class BillyApiPoint
 * @package App\Billy
 */
class BillyApiPoint
{


    /**
     * @param $method
     * @param $url
     * @param $data
     * @return bool|mixed
     */
    public static function  request($method, $url, $data)
    {
        try {
            $c = curl_init("https://api.billysbilling.com/v2" . $url);
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);

            // Set headers
            curl_setopt($c, CURLOPT_HTTPHEADER, array(
                "X-Access-Token: " . Config::get('billy.access-token')
            ,
                "Content-Type: application/json"
            ));

            if ($data) {
                // Set body
                curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($data));
            }

            // Execute request
            $res = curl_exec($c);
            $status = curl_getinfo($c, CURLINFO_HTTP_CODE);
            $body = json_decode($res);
            if ($status >= 400) {
                throw new Exception("$method: $url failed with $status - $res");
            }
            return $body;
        } catch (Exception $e) {
            dd($data,$e);
            return false;
        }
    }
}