<?php

namespace App\Models;

use App\Validators\ProductsValidation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Billy\BillyApiPoint;
use Illuminate\Support\Facades\DB;
use TheSeer\Tokenizer\Exception;

/**
 * Class ProductsModel
 * @package App\Models
 */
class ProductsModel extends Model
{
    use HasFactory;


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllProducts()
    {
        return DB::table('products')->select('tableID', 'name')->get();
    }

    /**
     * @param $id
     * @return Model|null|object|static
     */
    public function getCurrentProducts($id)
    {
        return DB::table('products')
            ->select(
                "organizationId",
                "name",
                "description",
                "accountId",
                "productNo",
                "suppliersProductNo",
                "salesTaxRulesetId",
                "isArchived",
                "prices")
            ->where('tableId', $id)->first();

    }

    /**
     * @param $data
     * @param $isAdd
     * @return bool
     */
    public function editAddCustomer($data, $isAdd)
    {
        if (isset($data['_token']))
            unset($data['_token']);
        if ($isAdd == false) {
            $tableId = $data['tableID'];
            unset($data['tableID']);
        }
        $validator = new ProductsValidation();


        if ($isAdd == false) {
            $validationResponse = $validator->productsEditValidation($data);

            if ($validationResponse === true) {
                $data['prices'] = [array(
                    'unitPrice' => $data['prices'],
                    'currencyId' => 'DKK'
                )];
                $recordId = DB::table('products')->select('id')->where('tableID', $tableId)->first();
                BillyApiPoint::request('PUT', '/products/' . $recordId->id, ['product' => $data]);
                DB::table('products')->where('tableID', $tableId)->update($data);
            } else {
                return $validationResponse;
            }

        } else {
            $validationResponse = $validator->productsAddValidation($data);

            if ($validationResponse === true) {
                if (isset($data['id']))
                    unset($data['id']);
                if (isset($data['createdTime']))
                    unset($data['createdTime']);
                $data['prices'] = [array(
                    'unitPrice' => $data['prices'],
                    'currencyId' => 'DKK'
                )];

                $r = BillyApiPoint::request('POST', '/products/', ['product' => $data]);

                unset($r->products[0]->inventoryAccountId);
                unset($r->products[0]->isInInventory);
                unset($r->products[0]->imageId);
                unset($r->products[0]->imageUrl);

                DB::table('products')->insert(get_object_vars($r->products[0]));

                return true;

            } else {
                return $validationResponse;
            }
        }

        return false;
    }

    /**
     * @return bool|mixed
     */
    public function syncProducts()
    {
        $billyApi = BillyApiPoint::request('GET', '/products', []);
        if ($billyApi == false)
            return $billyApi;

        foreach ($billyApi->products as $key => $product) {
            unset($billyApi->products[$key]->inventoryAccountId);
            unset($billyApi->products[$key]->isInInventory);
            unset($billyApi->products[$key]->imageId);
            unset($billyApi->products[$key]->imageUrl);
        }
        $productsIDs = [];
        foreach ($billyApi->products as $ba) {
            array_push($productsIDs, $ba->id);
        }

        $existedIds = DB::table('products')->whereIn('id', $productsIDs)->select('id')->get();

        // I don`t know what exactly mean sync from billy and for that reason logic here is to delete existed products which is have same id
        // like products from billy (this is for case if we have product which is changed but not updated in Billy) and  insert missing products
        try {
            if (count($existedIds) > 0) {
                DB::table('products')->whereIn('id', json_decode(json_encode($existedIds), true))->delete();
            }

            DB::table('products')->insert(json_decode(json_encode($billyApi->products), true));
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

}
