<?php

namespace App\Models;

use App\Billy\BillyApiPoint;
use App\Validators\ContactsValidation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use TheSeer\Tokenizer\Exception;

/**
 * Class ContactsModel
 * @package App\Models
 */
class ContactsModel extends Model
{
    use HasFactory;


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllContacts()
    {
        return DB::table('contacts')->select('tableID', 'name')->get();
    }

    /**
     * @param $id
     * @return Model|null|object|static
     */
    public function getCurrentContacts($id)
    {
        return DB::table('contacts')
            ->select(
                "type",
                "organizationId",
                "createdTime",
                "name",
                "countryId",
                "street",
                "cityId",
                "cityText",
                "stateId",
                "stateText",
                "zipcodeText",
                "contactNo",
                "phone",
                "fax",
                "currencyId",
                "registrationNo",
                "ean",
                "localeId",
                "isCustomer",
                "isSupplier",
                "paymentTermsMode",
                "paymentTermsDays",
                "accessCode",
                "emailAttachmentDeliveryMode",
                "isArchived",
                "isSalesTaxExempt",
                "defaultExpenseProductDescription",
                "defaultExpenseAccountId",
                "defaultTaxRateId")
            ->where('tableId', $id)->first();

    }

    /**
     * @param $data
     * @param $isAdd
     * @return bool
     */
    public function editAddCustomer($data, $isAdd)
    {
        unset($data['_token']);
        $tableId = $data['tableID'];
        unset($data['tableID']);
        $validator = new ContactsValidation();


        if ($isAdd == false) {
            $validationResponse = $validator->contactsEditValidation($data);

            if ($validationResponse === true) {
                $reconrdId = DB::table('contacts')->select('id')->where('tableID', $tableId)->first();

                BillyApiPoint::request('PUT', '/contacts/' . $reconrdId->id, ['contact' => $data]);
                DB::table('contacts')->where('tableID', $tableId)->update($data);
                return true;
            } else {
                return $validationResponse;
            }
        } else {
            $validationResponse = $validator->contactsAddValidation($data);

            if ($validationResponse === true) {
                if(isset($data['id']))
                    unset($data['id']);
                if(isset($data['createdTime']))
                    unset($data['createdTime']);
                BillyApiPoint::request('POST', '/contacts/', ['contact' => $data]);

                DB::table('contacts')->insert($data);
                return true;

            } else {

                return $validationResponse;
            }
        }
    }

    /**
     * @return bool|mixed
     */
    public function syncContacts()
    {
        $billyApi = BillyApiPoint::request('GET', '/contacts', []);
        if ($billyApi == false)
            return $billyApi;

        $contactsIDs = [];
        foreach ($billyApi->contacts as $ba) {
            array_push($contactsIDs, $ba->id);
        }

        $existedIds = DB::table('contacts')->whereIn('id', $contactsIDs)->select('id')->get();

        // I don`t know what exactly mean sync from billy and for that reason logic here is to delete existed contacts which is have same id
        // like contacts from billy (this is for case if we have contact which is changed but not updated in Billy) and  insert missing contacts
        try {
            if (count($existedIds) > 0) {
                DB::table('contacts')->whereIn('id', json_decode(json_encode($existedIds), true))->delete();
            }

            DB::table('contacts')->insert(json_decode(json_encode($billyApi->contacts), true));
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
}
