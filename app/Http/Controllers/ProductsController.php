<?php


namespace App\Http\Controllers;

use App\Models\ProductsModel;
use Illuminate\Http\Request;

/**
 * Class ProductsController
 * @package App\Http\Controllers
 */
class ProductsController extends Controller
{


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexPage()
    {
        $model = new ProductsModel();
        $products = $model->getAllProducts();
        return view('products.index', ['products'=> $products]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getProducts(Request $request){
        $model = new ProductsModel();
        $contact = $model->getCurrentProducts($request->get('id'));
        $contact = get_object_vars($contact);
        return response($contact);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function editProducts(Request $request)
    {
        $res = $this->editCreateProducts($request,  false);
        if($res === true){
            return redirect()->route('products');
        }elseif($request === false) {
            return redirect()->route('products')->withErrors(['error', 'something wrong']);
        }

        return redirect()->route('products')->withErrors($res);

    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function createProducts(Request $request)
    {
        $res = $this->editCreateProducts($request, true);
        if($res === true){
            return redirect()->route('products');
        }elseif($request === false) {
            return redirect()->route('products')->withErrors(['error', 'something wrong']);
        }

        return redirect()->route('products')->withErrors($res);

    }

    /**
     * @param $request
     * @param $type
     * @return bool
     */
    private function editCreateProducts($request, $type){
        $model = new ProductsModel();
        $res = $model->editAddCustomer($request->all(), $type);

        return $res;

    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function syncProducts()
    {
        $model = new ProductsModel();
        $res = $model ->syncProducts();
        if($res == false){
            return redirect()->route('products')->withErrors(['error', 'Sync error.']);
        }
        return redirect()->route('products');

    }
}
