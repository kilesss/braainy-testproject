<?php

namespace App\Http\Controllers;

use App\Models\ContactsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

/**
 * Class ContactsController
 * @package App\Http\Controllers
 */
class ContactsController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function indexPage()
    {
        $model = new ContactsModel();
        $contacts = $model->getAllContacts();
        return view('contacts.index', ['contacts'=> $contacts]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getContacts(Request $request){
        $model = new ContactsModel();
        $contact = $model->getCurrentContacts($request->get('id'));
        $contact = get_object_vars($contact);
        return response($contact);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function editContacts(Request $request)
    {
        $res = $this->editCreateContacts($request,  false);
        if($res === true){
            return redirect()->route('contacts');
        }elseif($request === false) {
            return redirect()->route('contacts')->withErrors(['error', 'something wrong']);
        }

        return redirect()->route('contacts')->withErrors($res);

    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function createContacts(Request $request)
    {
        $res = $this->editCreateContacts($request, true);
        if($res === true){
            return redirect()->route('contacts');
        }elseif($request === false) {
            return redirect()->route('contacts')->withErrors(['error', 'something wrong']);
        }

        return redirect()->route('contacts')->withErrors($res);

    }

    /**
     * @param $request
     * @param $type
     * @return bool
     */
    private function editCreateContacts($request, $type){
        $model = new ContactsModel();
        $res = $model->editAddCustomer($request->all(), $type);

        return $res;

    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function syncContacts(Request $request)
    {
        $model = new ContactsModel();
        $res = $model ->syncContacts();
        if($res == false){
            return redirect()->route('contacts')->withErrors(['error', 'Sync error.']);
        }
        return redirect()->route('contacts');

    }
}
