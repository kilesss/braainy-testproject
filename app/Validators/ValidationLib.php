<?php

namespace App\Validators;
use Illuminate\Support\Facades\Validator;

/**
 * Created by PhpStorm.
 * User: kilesss
 * Date: 10/10/2020
 * Time: 8:12 PM
 */
class ValidationLib
{


    /**
     * @param $request
     * @param $rules
     * @return bool
     */
    protected function validate($request, $rules){

        $messages = $this->messagesFields($rules);
        $validator = Validator::make($request,$rules, $messages);
        if ($validator->fails()) {
            return $validator->messages()->getMessages();
        }
        return true;
    }

    /**
     * @param $arrRules
     * @return array
     */
    protected function rules($arrRules)
    {

        $arr = [];
        foreach ($arrRules as $rule) {
            $arr[$rule] = $this->rulesValues[$rule];
        }
        return $arr;
    }


    /**
     * @param $field
     * @return array
     */
    protected function messagesFields($field)
    {

        $arr = [];
        foreach ($field as $key => $fil) {
            if (strpos($fil, '|') !== false) {
                foreach (explode('|', $fil) as $rule) {
                    $arr[$key . "." . $rule] = $key . "_" . $rule;
                }
            }else{
                //use in case like date_format
                $rule = explode(':', $fil);
                $arr[$key . "." . $rule[0]] = $key . "_" . $rule[0];
            }
        }
        return $arr;
    }

}
