<?php
/**
 * Created by PhpStorm.
 * User: kilesss
 * Date: 10/10/2020
 * Time: 8:18 PM
 */

namespace App\Validators;

/**
 * Class ContactsValidation
 * @package App\Validators
 */
class ContactsValidation extends ValidationLib
{


    /**
     * @param $data
     * @return bool
     */
    public function contactsEditValidation($data)
    {
        $fields = [
            "type",
            "organizationId",
            "name",
            "countryId",
            "street",
            "cityId",
            "cityText",
            "stateId",
            "stateText",
            "zipcodeText",
            "contactNo",
            "phone",
            "fax",
            "currencyId",
            "registrationNo",
            "ean",
            "localeId",
            "isCustomer",
            "isSupplier",
            "paymentTermsMode",
            "paymentTermsDays",
            "accessCode",
            "emailAttachmentDeliveryMode",
            "isArchived",
            "isSalesTaxExempt",
            "defaultExpenseProductDescription",
            "defaultExpenseAccountId",
            "defaultTaxRateId"];
        return $this->validate(
            $data,
            $this->rules($fields)
        );
    }

    /**
     * @param $data
     * @return bool
     */
    public function contactsAddValidation($data)
    {


        $fields = [
            "type",
            "organizationId",
            "name",
            "countryId",
            "street",
            "cityId",
            "cityText",
            "stateId",
            "stateText",
            "zipcodeText",
            "contactNo",
            "phone",
            "fax",
            "currencyId",
            "registrationNo",
            "ean",
            "localeId",
            "isCustomer",
            "isSupplier",
            "paymentTermsMode",
            "paymentTermsDays",
            "accessCode",
            "emailAttachmentDeliveryMode",
            "isArchived",
            "isSalesTaxExempt",
            "defaultExpenseProductDescription",
            "defaultExpenseAccountId",
            "defaultTaxRateId"];
        return $this->validate(
            $data,
            $this->rules($fields)
        );
    }

    /**
     * @var array
     */
    protected $rulesValues = [
        "type" => 'required|string',
        "organizationId" => 'required|string',
        "name" => 'required|string',
        "countryId" => 'required|string',
        "street" => 'string|nullable',
        "cityId" => 'numeric|nullable',
        "cityText" => 'string|nullable',
        "stateId" => 'numeric|nullable',
        "stateText" => 'string|nullable',
        "zipcodeText" => 'string|nullable',
        "contactNo" => 'string|nullable',
        "phone" => 'string|nullable',
        "fax" => 'string|nullable',
        "currencyId" => 'string|nullable',
        "registrationNo" => 'required|string',
        "ean" => 'string|nullable',
        "localeId" => 'numeric|nullable',
        "isCustomer" => 'required|min:0|max:1',
        "isSupplier" => 'required|min:0|max:1',
        "paymentTermsMode" => 'string|nullable',
        "paymentTermsDays" => 'string|nullable',
        "accessCode" => 'required|string',
        "emailAttachmentDeliveryMode" => 'string|nullable',
        "isArchived" => 'required|min:0|max:1',
        "isSalesTaxExempt" => 'required|min:0|max:1',
        "defaultExpenseProductDescription" => 'string|nullable',
        "defaultExpenseAccountId" => 'string|nullable',
        "defaultTaxRateId" => 'string|nullable',
    ];
}