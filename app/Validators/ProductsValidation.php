<?php
/**
 * Created by PhpStorm.
 * User: kilesss
 * Date: 10/10/2020
 * Time: 8:18 PM
 */

namespace App\Validators;

/**
 * Class ProductsValidation
 * @package App\Validators
 */
class ProductsValidation extends ValidationLib
{


    /**
     * @param $data
     * @return bool
     */
    public function productsEditValidation($data)
    {


        $fields = [
            "organizationId",
            "name",
            "description",
            "accountId",
            "productNo",
            "suppliersProductNo",
            "salesTaxRulesetId",
            "isArchived",
            "prices"
        ];
        return $this->validate(
            $data,
            $this->rules($fields)
        );
    }

    /**
     * @param $data
     * @return bool
     */
    public function productsAddValidation($data)
    {


        $fields = [
            "organizationId",
            "name",
            "description",
            "accountId",
            "productNo",
            "suppliersProductNo",
            "salesTaxRulesetId",
            "isArchived",
            "prices",
        ];

        return $this->validate(
            $data,
            $this->rules($fields)
        );
    }

    /**
     * @var array
     */
    protected $rulesValues = [
        "organizationId" => 'required|string',
        "name" => 'required|string',
        "description" => 'string|nullable',
        "accountId" => 'required|string',
        "productNo" => 'string',
        "suppliersProductNo" => 'string',
        "salesTaxRulesetId" => 'string',
        "isArchived" => 'string',
        "prices" => 'string',
    ];
}