<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ContactsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(['prefix'=>'/products'], function(){
        Route::get('/', [ProductsController::class, 'indexPage'])->name('products');
        Route::get('/getProducts', [ProductsController::class, 'getProducts'])->name('getProducts');

        Route::post('/edit',[ProductsController::class, 'editProducts'])->name('products/edit');
        Route::post('/create',[ProductsController::class, 'createProducts'])->name('products/create');
        Route::get('/sync',[ProductsController::class, 'syncProducts'])->name('products/sync');
    });

    Route::group(['prefix'=>'/contacts'], function(){
        Route::get('/', [ContactsController::class, 'indexPage'])->name('contacts');
        Route::get('/getContacts', [ContactsController::class, 'getContacts'])->name('getContacts');
        Route::post('/edit',[ContactsController::class, 'editContacts'])->name('contacts/edit');
        Route::post('/create',[ContactsController::class, 'createContacts'])->name('contacts/create');
        Route::get('/sync',[ContactsController::class, 'syncContacts'])->name('contacts/sync');
    });
});
